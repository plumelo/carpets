# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0007_auto_20160212_0756'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='meta',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='carpet',
            name='image',
            field=models.ImageField(upload_to=b'carpet/images'),
        ),
    ]
