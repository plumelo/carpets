# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0021_auto_20160331_1142'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='color',
            options={'ordering': ['group']},
        ),
        migrations.RemoveField(
            model_name='color',
            name='order',
        ),
        migrations.AddField(
            model_name='colorgroup',
            name='order',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
