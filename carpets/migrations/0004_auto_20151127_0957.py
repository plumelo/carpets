# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0003_auto_20151127_0343'),
    ]

    operations = [
        migrations.AlterField(
            model_name='room',
            name='rotate_x',
            field=models.FloatField(default=70),
        ),
        migrations.AlterField(
            model_name='room',
            name='rotate_y',
            field=models.FloatField(default=50),
        ),
        migrations.AlterField(
            model_name='room',
            name='scale',
            field=models.FloatField(default=1.6),
        ),
    ]
