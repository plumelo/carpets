# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0017_colorgroup'),
    ]

    operations = [
        migrations.AddField(
            model_name='color',
            name='group_obj',
            field=models.ForeignKey(to='carpets.ColorGroup', null=True),
        ),
    ]
