# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0020_remove_color_group'),
    ]

    operations = [
        migrations.RenameField(
            model_name='color',
            old_name='group_obj',
            new_name='group',
        ),
    ]
