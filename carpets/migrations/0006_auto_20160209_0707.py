# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import carpets.models


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0005_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name': 'Category', 'verbose_name_plural': 'Categories'},
        ),
        migrations.AddField(
            model_name='room',
            name='categories',
            field=models.ManyToManyField(to='carpets.Category'),
        ),
        migrations.AlterField(
            model_name='carpet',
            name='image',
            field=models.ImageField(upload_to=b'carpet/images', validators=[carpets.models.validate_image]),
        ),
        migrations.AlterField(
            model_name='room',
            name='rotate_x',
            field=models.FloatField(default=70, verbose_name=b'Perspective'),
        ),
        migrations.AlterField(
            model_name='room',
            name='rotate_y',
            field=models.FloatField(default=50, verbose_name=b'Horizon'),
        ),
    ]
