# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0024_auto_20160331_1247'),
    ]

    operations = [
        migrations.AddField(
            model_name='carpet',
            name='meta_description',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='carpet',
            name='meta_keywords',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='category',
            name='meta_description',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='category',
            name='meta_keywords',
            field=models.TextField(null=True, blank=True),
        ),
    ]
