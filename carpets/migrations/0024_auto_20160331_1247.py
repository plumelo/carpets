# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0023_add_id_to_order_field'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='color',
            options={},
        ),
        migrations.AlterModelOptions(
            name='colorgroup',
            options={'ordering': ['order']},
        ),
        migrations.AlterField(
            model_name='color',
            name='group',
            field=models.ForeignKey(to='carpets.ColorGroup'),
        ),
        migrations.AlterField(
            model_name='colorgroup',
            name='description',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='colorgroup',
            name='order',
            field=models.IntegerField(unique=True),
        ),
    ]
