# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def set_order(apps, schema_editor):
    Color = apps.get_model("carpets", "Color")
    for i, color in enumerate(Color.objects.all()):
        color.order = i
        color.save()


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0014_auto_20160315_1621'),
    ]

    operations = [
        migrations.RunPython(set_order),
    ]
