# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0019_add_fk_to_group'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='color',
            name='group',
        ),
    ]
