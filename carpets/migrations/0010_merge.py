# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0006_auto_20151204_0220'),
        ('carpets', '0009_auto_20160301_0531'),
    ]

    operations = [
    ]
