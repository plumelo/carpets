# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0013_auto_20160315_1558'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='color',
            options={'ordering': ['order']},
        ),
        migrations.AddField(
            model_name='color',
            name='order',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
