$(document).ready(function () {
    $(document).on('click', '#print-btn', function () {
        var $this = $(this);
        var imageUrl = $('#preview').attr('src');
        var category = $this.data('carpet-category');
        var json = location.href.split("#")[1];
        if(json){
            var obj = $.parseJSON(json);
        }else{
            obj = {
                from_colors: [],
                to_colors: []
            }
        }
        var from = obj.from_colors;
        var to = obj.to_colors;

        var o_from = $('.change_color_item').map(function(i,e){
          var data = $(e).data();
          return {
            name    : data.name && data.name.toString(),
            hex     : data.value && data.value.toString(),
            o_name  : $(e).find('.color_name').text()
          }
        });
        var o_to = o_from.map(function(i,c){
          return {
            hex     :  (to[from.indexOf(c.hex)] || c.hex).toString(),
            name    :   c.o_name
          }


        }).toArray();

        o_from = o_from.toArray();

        var carpetName = $this.data('carpet-name');
        //var image = $('<image src="' + imageUrl + '">');
        var image = $('<img>');
        var imageContainer = $('<div class="" id="img-container" style=""></div>');
        var colors = '';
        $.each(o_from.slice(1), function (i, item) {
            colors += '<tr>'
                + '<td></td><td></td>'
                + '<td>' + item.name + ' ' +'<span style="color: ' + '#' + item.hex + '!important;' + '  ">&#9608;&#9608;&#9608;&#9608;&#9608;</span>' + '</td>'
                + '<td>' + o_to[i + 1].name + ' ' + '<span style="color: ' + '#' + o_to[i + 1].hex + '!important;' + '  ">&#9608;&#9608;&#9608;&#9608;&#9608;</span>' +'</td>'
                + '</tr>';
        });
        var table = '<table class="table table-bordered" border="1">' + '<tbody>' + '<tr>' +
            '<th>Category</th>' +
            '<th>Name</th>' +
            '<th>From color</th>' +
            '<th>To color</th>' +
            '</tr>' +
            '<tr>' +
            '<td>' + category + '</td>' +
            '<td>' + carpetName + '</td>' +
            '<td>' + o_from[0].name + ' ' + '<span style="color: ' + '#' + o_from[0].hex + '!important;' + '  ">&#9608;&#9608;&#9608;&#9608;&#9608;</span>' + '</td>' +
            '<td>' + o_to[0].name + ' ' + '<span style="color: ' + '#' + o_to[0].hex + '!important;' + '  ">&#9608;&#9608;&#9608;&#9608;&#9608;</span>' + '</td>' +
            '</tr>' + colors +
            '</tbody>' + '</table>';
        imageContainer.append(table);
        imageContainer.append(image);
        image.load(function () {
            imageContainer.print({
                iframe: true,
                timeout: 2000,
            });
            //imageContainer.remove();
        });
        //$('body').append(imageContainer);
        image.attr('src', imageUrl + '&stamp=' + (new Date()).getTime() );
    });
});
