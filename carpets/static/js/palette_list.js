$(document).ready(function () {
    $(document).on('click', '#btnPrint', function () {
        var $this = $(this);
        var imageUrl = $this.data('image-url');
        var id = $this.data('palette-id');
        var data = $this.data('palette-json');
        var json = data.split('?hash=')[1];
        var obj = $.parseJSON(json);
        var from = obj.from_colors;
        var to = obj.to_colors;
        var carpetName = $this.data('carpet-name');
        var image = $('<image src="' + imageUrl + '">');
        var imageContainer = $('<div class="" id="image-container" style=""></div>');
        var colors = '';
        $.each(from.slice(1), function (i, item) {
           colors += '<tr>'
                + '<td></td><td></td>'
                + '<td>' + item + ' ' +'<span style="color: ' + '#' + item + '!important;' + '  ">&#9608;&#9608;&#9608;&#9608;&#9608;</span>' + '</td>'
                + '<td>' + to[i + 1] + ' ' + '<span style="color: ' + '#' + to[i + 1] + '!important;' + '  ">&#9608;&#9608;&#9608;&#9608;&#9608;</span>' +'</td>'
                + '</tr>';
        });
        var table = '<table class="table table-bordered" border="1">' + '<tbody>' + '<tr>' +
            '<th>ID</th>' +
            '<th>Name</th>' +
            '<th>From color</th>' +
            '<th>To color</th>' +
            '</tr>' +
            '<tr><td>' + id + '</td><td>' + carpetName + '</td><td>' + from[0] + ' ' +  '<span style="color: ' + '#' + from[0] + '!important;' + '  ">&#9608;&#9608;&#9608;&#9608;&#9608;</span>' + '</td>' +
            '<td>' + to[0] + ' ' + '<span style="color: ' + '#' + to[0] + '!important;' + '  ">&#9608;&#9608;&#9608;&#9608;&#9608;</span>' + '</td>' +
            '</tr>'
            + colors +
            '</tbody>' + '</table>';
        imageContainer.append(table);
        imageContainer.append(image);
        image.load(function () {
            $('#image-container').print();
            $('#image-container').remove();
        });
        $('body').append(imageContainer);
        image.attr('src', imageUrl)
    })
});