from django.conf.urls import patterns,  url
from django.contrib.auth.decorators import login_required
from carpets.views import (replace_colors, recolor_carpet,
                           room_preview, palette_create_ajax_view, download_pcx,
                           CategoryListView, PaletteListView, PaletteDeleteView, tune_room,
                           search)
from views import logout_ajax

urlpatterns = patterns('',
    url(r'^categories/$', CategoryListView.as_view(), name='categories'),
    url(r'^palettes/$', login_required(PaletteListView.as_view(), login_url='/'), name='palettes'),
    url(r'^recolor_carpet/(?P<item_code>.*)/$', recolor_carpet, name='recolor_carpet'),
    url(r'^replace_colors/(?P<item_code>.*)/(?P<room_id>.*)/$', room_preview, name='room_preview'),
    url(r'^replace_colors/(?P<item_code>.*)/$', replace_colors, name='replace_colors'),
    url(r'^save_palette', palette_create_ajax_view, name='save_palette'),
    url(r'^download_pcx/(?P<pk>\d+)/$', download_pcx, name='download_pcx'),
    url(r'^tune_room/(?P<room_id>.*)/$', tune_room, name='tune_room'),
    # url(r'^room_preview/(?P<pk>\d+)/$', room_preview, name='room_preview'),
    url(r'^palette/(?P<pk>\d+)/delete/$', login_required(PaletteDeleteView.as_view(), login_url='/'), name='delete_palette'),
    url(r'^accounts/logout/$', logout_ajax, name='logout'),
    url(r'^search/', search, name='search'),
    url(r'^(?P<url>.+)$', CategoryListView.as_view(), name='category_details'),
)
